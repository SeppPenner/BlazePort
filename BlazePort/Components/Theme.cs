﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazePort.Components
{
    public static class Theme
    {
        public const string Success = "success";

        public const string Warning = "warning";

        public const string Danger = "danger";

        public const string Info = "info";

    }
}
